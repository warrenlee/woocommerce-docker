FROM php:8.0-apache

#ENV WOOCOMMERCE_VERSION 2.6.1
#ENV WOOCOMMERCE_UPSTREAM_VERSION 2.6.1

# install the PHP extensions we need
RUN apt-get update; \
    apt-get install -y --no-install-recommends \
        libfreetype6-dev \
        libjpeg-dev \
        libpng-dev \
        libzip-dev \
        supervisor \
        cron \
    && docker-php-ext-configure gd \
        --with-freetype \
        --with-jpeg \
    && docker-php-ext-install -j "$(nproc)" \
        bcmath \
        exif \
        gd \
        mysqli \
        zip

# Install WP-CLI (http://wp-cli.org)
RUN curl -o /bin/wp https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar \
    && chmod +x /bin/wp

# Install dockerize (https://github.com/jwilder/dockerize)
ENV DOCKERIZE_RELEASE v0.2.0/dockerize-linux-amd64-v0.2.0.tar.gz
RUN curl -sL https://github.com/jwilder/dockerize/releases/download/${DOCKERIZE_RELEASE} \
    | tar -C /usr/bin -xzvf -

ENV SUPERCRONIC_URL=https://github.com/aptible/supercronic/releases/download/v0.1.12/supercronic-linux-amd64 \
    SUPERCRONIC=supercronic-linux-amd64 \
    SUPERCRONIC_SHA1SUM=048b95b48b708983effb2e5c935a1ef8483d9e3e

# Install Supercronic
RUN curl -fsSLO "$SUPERCRONIC_URL" \
 && echo "${SUPERCRONIC_SHA1SUM}  ${SUPERCRONIC}" | sha1sum -c - \
 && chmod +x "$SUPERCRONIC" \
 && mv "$SUPERCRONIC" "/usr/local/bin/${SUPERCRONIC}" \
 && ln -s "/usr/local/bin/${SUPERCRONIC}" /usr/local/bin/supercronic

# Clean up
RUN apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Create a directory for the WordPress installation
RUN mkdir -p /var/www/html
WORKDIR /var/www/html

RUN a2enmod rewrite expires

# Setup cron
COPY cron.conf /etc/cron.d/wp
RUN chmod 0644 /etc/cron.d/wp

# Create a user to run wp-cli commands (running as root is strongly discouraged)
# @see https://github.com/wp-cli/wp-cli/pull/973#issuecomment-35842969
RUN useradd -d /var/www/html -s /bin/bash wp \
    && chown -R wp .

USER wp

RUN crontab /etc/cron.d/wp

# Download Wordpress
RUN wp core download

# Copy wp-content folder
COPY ./wordpress/wp-content /var/www/html/wp-content

# Configure apache
RUN { \
        echo '# BEGIN WordPress'; \
        echo ''; \
        echo 'RewriteEngine On'; \
        echo 'RewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}]'; \
        echo 'RewriteBase /'; \
        echo 'RewriteRule ^index\.php$ - [L]'; \
        echo 'RewriteCond %{REQUEST_FILENAME} !-f'; \
        echo 'RewriteCond %{REQUEST_FILENAME} !-d'; \
        echo 'RewriteRule . /index.php [L]'; \
        echo ''; \
        echo '# END WordPress'; \
        echo 'SetEnvIf Authorization "(.*)" HTTP_AUTHORIZATION=$1'; \
    } > .htaccess

EXPOSE 80

ADD supervisor.conf /etc/supervisor.conf

CMD wp core config \
    --dbhost=${MYSQL_HOST:-mysql}:${MYSQL_PORT:-3306} \
    --dbuser=${MYSQL_USER:-root} \
    --dbpass=${MYSQL_PASSWORD:-root} \
    --dbname=${MYSQL_DATABASE:-wp} \
    --skip-check \
    --force \
# Wait for the database to be ready
&& dockerize -wait tcp://${MYSQL_HOST:-mysql}:${MYSQL_PORT:-3306} -timeout 30s \
# Configure site
&& wp core install \
    --url=${WORDPRESS_URL:-localhost:8080} \
    --title=${WORDPRESS_TITLE:-'WordPress site'} \
    --admin_user=${WORDPRESS_ADMIN_USER:-admin} \
    --admin_password=${WORDPRESS_ADMIN_PASSWORD:-admin} \
    --admin_email=${WORDPRESS_ADMIN_MAIL:-admin@example.com} \
    --skip-email \
# Activate theme
#&& wp theme activate twentytwentyone-child \
# Set permalink structure
&& wp option update permalink_structure '/%year%/%monthnum%/%day%/%postname%/' \
&& wp plugin install \
    # Install woocommerce
    woocommerce \
    # Install wordpress-importer
    wordpress-importer \
    # Woocommerce Stripe Gateway
    woocommerce-gateway-stripe \
    woo-gutenberg-products-block\
    --activate \
# Import sample products
&& wp import wp-content/plugins/woocommerce/sample-data/sample_products.xml \
    --authors=skip \
&& supervisord -c /etc/supervisor.conf
