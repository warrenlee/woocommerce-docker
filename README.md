## Docker: Woocommerce

Have docker installed

In the project folder run the following

#### Build

```shell
docker-compose build
```

#### Run

```shell
docker-compose up
```

Visit http://localhost:8080/wp-admin

```
Username: admin
Password admin
```

### Create REST API 

http://localhost:8080/wp-admin/admin.php?page=wc-settings&tab=advanced&section=keys&create-key=1

Go here to create API permission. Make note of the key and secret.

```
Description: NuxtJs
User: Admin
Permissions: Read
```
